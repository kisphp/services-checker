.PHONY: test svc

network = kisphp

test:
	@docker build -t demo-php .
	@docker run --rm --env-file .env --network="kisphp" -it demo-php bash -c "php run.php"

svc: pgsql mysql redis elk

pgsql:
	docker run \
		--rm \
		-d \
		--name docker_pgsql \
		--net "$(network)" \
		-e POSTGRES_PASSWORD=kisphp \
		-e POSTGRES_DB=kisphp \
		-e POSTGRES_USER=kisphp \
		-p "5432:5432" \
		-t postgres:11

mysql:
	docker run \
		--rm \
		-d \
		-p "3306:3306" \
		--name docker_mysql \
		--net $(network) \
		-e MYSQL_DATABASE=kisphp \
		-e MYSQL_ROOT_PASSWORD=kisphp \
		-e MYSQL_USER=kisphp \
		-e MYSQL_PASSWORD=kisphp \
		-t mysql:5.7

redis:
	docker run \
		--rm \
		-d \
		-p "6379:6379" \
		--name docker_redis \
		--net $(network) \
		-t redis:6

elk:
	docker run \
	--rm \
	-d \
	--name docker_elasticsearch \
	--net $(network) \
	-p 9200:9200 \
	-p 9300:9300 \
	-e "discovery.type=single-node" \
	elasticsearch:5.6
