<?php

namespace Kisphp;

use Symfony\Component\Console\Helper\Table;

abstract class AbstractConnector
{
    private const YES = 'YES';
    private const NO = 'NO';

    /**
     * @param Table $table
     *
     * @return array
     */
    public function checkConnection(Table $table)
    {
        if ( empty($this->getEntrypoint())) {
            return [];
        }

        try {
            $this->serviceCheck();

            $service = $this->writeSuccess($this->getServiceName());
            $status = $this->writeSuccess(self::YES);
            $message = '';
        } catch (\Exception $e) {
            $service = $this->writeError($this->getServiceName());
            $status = $this->writeError(self::NO);
            $message = $e->getMessage();
        }

        $table->addRow([
            $service,
            $this->getEntrypoint(),
            $this->getInfo(),
            $status,
            $message,
        ]);
    }

    abstract protected function serviceCheck();

    /**
     * @return string
     */
    abstract protected function getEntrypoint(): string;

    /**
     * @return string
     */
    abstract protected function getInfo(): string;

    /**
     * @return string
     */
    protected function getServiceName()
    {
        $items = explode('\\', get_called_class());
        $serviceClassName = end($items);
        $serviceClassName = str_replace('Connector', '', $serviceClassName);

        return $serviceClassName;
    }

    /**
     * @param string $text
     *
     * @return string
     */
    protected function writeError($text)
    {
        return '<fg=red>'.$text.'</>';
    }

    /**
     * @param string $text
     *
     * @return string
     */
    protected function writeSuccess($text)
    {
        return '<fg=green>'.$text.'</>';
    }
}
