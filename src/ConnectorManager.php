<?php

namespace Kisphp;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;

class ConnectorManager
{
    /**
     * @var Table
     */
    protected $table;

    public function __construct()
    {
        $output = new ConsoleOutput();

        $this->table = new Table($output);
        $this->table->setHeaders([
            'Service',
            'Host',
            'Data',
            'Connected',
            'Message',
        ]);

        $this->checkConnectors();
    }

    public function show()
    {
        $this->table->render();
    }

    protected function checkConnectors()
    {
        /** @var \DirectoryIterator $item */
        foreach (new \DirectoryIterator(__DIR__ . '/Connectors') as $item) {
            if ($item->isDot()) {
                continue;
            }
            $className = sprintf('Kisphp\\Connectors\\%s', str_replace('.php', '', $item->getBasename()));

            /** @var AbstractConnector $obj */
            $obj = new $className();
            $obj->checkConnection($this->table);
        }
    }
}
