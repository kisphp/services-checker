<?php

namespace Kisphp\Connectors;

use Kisphp\AbstractConnector;
use Predis\Client;

class RedisConnector extends AbstractConnector
{
    /**
     * @var \Redis
     */
    protected $redis;

    protected $info = [];

    protected function serviceCheck()
    {
        $this->redis = new \Redis();
        $this->redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT'));

        $this->info = $this->redis->info();
    }

    protected function getEntrypoint(): string
    {
        return getenv('REDIS_HOST');
    }

    protected function getInfo(): string
    {
        return $this->info['redis_version'] . ' ' . $this->info['redis_mode'];
    }
}
