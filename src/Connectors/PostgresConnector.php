<?php

namespace Kisphp\Connectors;

use Kisphp\AbstractConnector;

class PostgresConnector extends AbstractConnector
{
    const SERVICE = 'postgres';

    protected function serviceCheck()
    {
        $dsn = sprintf(
            'pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s',
            getenv('PGSQL_HOST'),
            getenv('PGSQL_PORT'),
            getenv('PGSQL_DATABASE'),
            getenv('PGSQL_USER'),
            getenv('PGSQL_PASSWORD')
        );


        $con = new \PDO($dsn);
    }

    protected function getEntrypoint(): string
    {
        return getenv('PGSQL_HOST');
    }

    protected function getInfo(): string
    {
        return getenv('PGSQL_USER') . '@' . getenv('PGSQL_DATABASE');
    }
}
