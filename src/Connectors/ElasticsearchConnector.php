<?php

namespace Kisphp\Connectors;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Kisphp\AbstractConnector;

class ElasticsearchConnector extends AbstractConnector
{
    /**
     * @var Client
     */
    protected $client;

    protected $info;

    protected function serviceCheck()
    {
        $hosts = [
            'host' => getenv("ELK_HOST"),
            'port' => getenv("ELK_PORT"),
            'schema' => 'http',
        ];

        $this->client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        $this->info = $this->client->info();
    }

    protected function getEntrypoint(): string
    {
        return getenv('ELK_HOST');
    }

    protected function getInfo(): string
    {
        if (is_null($this->info)) {
            return '';
        }

        return $this->info['version']['number'] . ' @ ' . $this->info['version']['build_date'];
    }
}
