<?php

namespace Kisphp\Connectors;

use Kisphp\AbstractConnector;

class MysqlConnector extends AbstractConnector
{
    const SERVICE = 'mysql';

    protected function serviceCheck()
    {
        $con = new \PDO(sprintf(
            'mysql:host=%s;port=%d;dbname=%s;user=%s;password=%s',
            getenv('MYSQL_HOST'),
            getenv('MYSQL_PORT'),
            getenv('MYSQL_DATABASE'),
            getenv('MYSQL_USER'),
            getenv('MYSQL_PASSWORD')
        ));
    }

    protected function getEntrypoint(): string
    {
        return getenv('MYSQL_HOST');
    }

    protected function getInfo(): string
    {
        return getenv('MYSQL_USER') . '@' . getenv('MYSQL_DATABASE');
    }
}
